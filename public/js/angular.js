console.log("......xxx");
var app = angular.module('ServerApp', []);
app.controller('ServerController', function($scope, $http) {
    $scope.contents = {};
    $http.get("/tester")
        .then(function(response) {
            //First function handles success
            $scope.contents = response.data;
        }, function(response) {
            //Second function handles error
            $scope.contents = "Something went wrong";
        });

    $scope.remove = function(index,companyID) {
        console.log(companyID);
        $scope.contents.splice(index, 1);

        $http.get("/tester/remove/" + companyID)
            .then(function (response) {
                alert("removed!")
            }, function (response) {
                console.log($scope.response.statusText)
            });
    }
});