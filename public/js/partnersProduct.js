console.log("*******");

var PartnerApp = angular.module('PartnerApp',[]);

PartnerApp.controller('PartnerController',function($scope,$http){

    $scope.products = {};

    $http.get("/products")
        .then(function(response){
            $scope.products = response.data;
        },function(response){
            console.log("error");
        });
    $scope.selectedProduct = $scope.products[0];


    $scope.removeProduct = function(index,id) {

        var confirm = window.confirm("Are you sure?");
        if (confirm == true) {
            console.log("INDEX" + index + id);
            // console.log("ID: " + $scope.xname);
            $scope.products.splice(index, 1);
            /*$http.get("/partners/remove/"+id)
             .then(function(response){
                console.log("Product Removed!")
             },function(response){
             console.log("error");
             });*/
        }
    }

    $scope.getProductInfo = function($index,id){
        console.log($scope.selectedProduct);
        console.log($index)
        // console.log("getting info to "+id)
        $http.get("/partners/product/"+id)
            .then(function(response){
                $scope.info = response.data
            },function(response){
                console.log("error");
            });
    }


});