<?php
//Controller Containers
/*Add User Controller Container*/
$container['BookingController'] = function($container){
    return new Nutcrack\Controllers\BookingController();
};
$container['UserController'] = function($container){
    return new Nutcrack\Controllers\UserController($container);
};
$container['ScanController'] = function($container){
    return new Nutcrack\Controllers\ScanController($container);
};
$container['TestController'] = function($container){
    return new Nutcrack\Controllers\TestController($container);
};
$container['BillingController'] = function($container){
    return new Nutcrack\Controllers\BillingController($container);
};
$container['PartnersController'] = function($container){
    return new Nutcrack\Controllers\PartnersController($container);
};
$container['PusherController'] = function($container){
    return new Nutcrack\Controllers\PusherController($container);
};

