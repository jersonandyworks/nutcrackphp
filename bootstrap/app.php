<?php
session_start();

/*THis will require all dependencies */
require __DIR__.'../../vendor/autoload.php';
// Import the necessary classes
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Query\Builder as DB;
// Create the Sentry alias
class_alias('Cartalyst\Sentry\Facades\Native\Sentry', 'Sentry');

$u_agent = $_SERVER["HTTP_USER_AGENT"];

if (preg_match('/windows|win32|win98|win95|win16/i', $u_agent)) {
	$operating_system = 'Windows';
}

if($operating_system !== "Windows"){
	$dir = $_SERVER['DOCUMENT_ROOT']."/../";
	$dotenv = new Dotenv\Dotenv($dir);
	$dotenv->load();
}

$dbName = $operating_system === "Windows" ? "trust_guard_dev" : $_ENV['DB_NAME'];
$userName = $operating_system === "Windows" ? "root" : $_ENV['DB_USER'];
$password = $operating_system === "Windows" ? "" : $_ENV['DB_PASS'];
//This will load the .env file

date_default_timezone_set('Asia/Singapore');

$capsule = new Capsule;

$capsule->addConnection([
	'driver'    => 'mysql',
	'host'      => 'localhost',
	'database'  => $dbName,
	'username'  => $userName,
	'password'  => $password,
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix'    => '',
]);

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();
// Configuration
$config = [
	'settings' => [
		'displayErrorDetails' => True
	]
];



// Instantiate the app object
$app = new \Slim\App($config);

$app->db = function(){
	return new Capsule;
};
// Get Container
$container = $app->getContainer();

// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__.'/../resources/views', [
        'cache' => false
    ]);
    // Instantiate and add Slim specific extension
	$basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    /*Add extensions for view here*/
	$view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    $view->addExtension(new Knlv\Slim\Views\TwigMessages(new Slim\Flash\Messages()));
    $view->addExtension(new Twig_Extension_StringLoader()); //this is need to be added, so that the twig template from the database will work!
	return $view;
};

$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

require  'containers.php';
require '../app/routes.php';