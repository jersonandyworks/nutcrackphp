<?php

namespace Nutcrack\Controllers;
use Nutcrack\Models\PartnerProduct as  PartnerProduct;
use Nutcrack\Models\Company as Company;
use Nutcrack\Models\Member as Member;
class PartnersController extends BaseController
{
    public function index($request,$response,$args){
        $products = PartnerProduct::all();
        $members = Company::where('companyPartnerID',1)->get();
        return $this->view->render($response,'partners.html',['products'=>$products,'members'=>$members]);
    }

    public function getProducts($request,$response,$args){
        $products = PartnerProduct::all();
        return $response->withJson($products);
    }

    public function addProduct($request,$response,$args){
        $d = $_POST;
        PartnerProduct::create(['memberID' => 2,
                                'productName' => $d['productName'],
                                'productDescription' => $d['productDescription'],
                                'planGeneratedID' => $d['planGeneratedID'],
                                'termType' => $d['termType'],
                                'holdDays' => $d['holdDays'],
                                'productType' => $d['productType'],
                                'scanType' => $d['scanType'],
                                'status' => "active",
                                'created' => date("Y-m-d h:i:s")
                                ]);
        return self::redirect($response,'/partners');
    }

    public function removeProduct($request,$response,$args){
        PartnerProduct::destroy($args['id']);
    }

    public function getProductInfo($request,$response,$args){
       $product =  PartnerProduct::find($args['id']);
       return $response->withJson($product);
    }

    public function server($request,$response,$args){
        return $this->view->render($response,'server.html');
    }

    public function test($request,$response,$args){
        $companies = Member::where('memberID',$args['id'])->first();
        /*$data = [];
        foreach($companies as $company){
            $data[] = ['companyID'   => $company->MemberId,
                       'companyName' => $company->FullName];
        }
*/
        return $response->withJson($companies)
            ->withHeader('Access-Control-Allow-Origin', 'http://webalizer.dev')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');;
    }
    public function remove($request,$response,$args){
        Member::destroy($args['id']);
        return true;
    }
}