<?php
/**
 * Created by PhpStorm.
 * User: andybarrios
 * Date: 03/12/2016
 * Time: 2:01 PM
 */
namespace Nutcrack\Controllers;
use Nutcrack\Models\Company;
use Nutcrack\Models\CompanyScanning;
use Nutcrack\Models\CompanySite;
use Nutcrack\Models\ScanResult;
use Slim\Views\Twig as View;
use Nutcrack\Models\User as User;
use Nutcrack\Models\ScanResult as Scan;
use Cartalyst\Sentry\Facades\Native\Sentry as Auth;
class TestController extends BaseController
{
    public function trialaccounts($request,$response,$args){
        $json = [];
        $trialAccounts = Company::getTrialMerchants();
       /* foreach($trialAccounts as $result){
            $json[] = [
                'companyID'     => $result->companyID,
                'companySiteID' => $result->companySiteID,
                'scanResultID'  => $result->scanResultID,
                'companyName'   => $result->companyName,
                'accountType'   => $result->accountType,
                'created'       => $result->created
            ];
        }*/
        return $response->withJson($trialAccounts);
    }
    public function newdevices($request,$response,$args){
        $json = [];
        $newDevices = Company::getNewDevices();

        foreach($newDevices as $result){
            $siteID = $result->siteID;
            $lastScan = ScanResult::lastScan($result->siteID);
            $reseller = !!$result->companyPartnerID ? Company::getCompany($result->companyPartnerID) : false;
            $json[] = ['companyName' => $result->companyName,
                       'reseller'    => $reseller->companyName,
                       'domain'      => $result->FullSite,
                       'lastScan'    => $lastScan->created
                      ];

        }
        return $response->withJson($json);
    }
    public function subscription($request,$response,$args){

        $json = [];
        $json[] = CompanyScanning::subscriptions();

        return $response->withJson($json);
    }
    public function test($request,$response,$args){
        $json = [];
        $companyID = CompanySite::where('siteID',7030)->value('companyID');
        $timeZone = Company::where('companyID',$companyID)->value('timeZone');
        $json[] = [
          'timestamp' => $timeZone
        ];
        return $response->withJson($json);
    }
    public function totaldomain($request,$response,$args){
        $json = [];
        $json[] = CompanyScanning::getTotalDomainByCompanyID(79);
        return $response->withJson($json);
    }
    public function timezone($request,$response,$args){
        $timeZone = "Europe/Moscow";
        $timeZonesStack = [
            "America/New_York"     => "[UTC -5 Eastern (New York)]",
            "America/Chicago"      => "[UTC -6 Central (Chicago)]",
            "America/Denver"       => "[UTC -7 Mountain (Denver)]",
            "America/Phoenix"      => "[UTC -7 Mountain (No DST)]",
            "America/Los_Angeles"  => "[UTC -8 Pacific(Los Angeles)]",
            "America/Anchorage"    => "[UTC -9 Alaska]",
            "America/Adak"         => "[UTC -10 Hawaii-Aleutian]",
            "Pacific/Honolulu"     => "[UTC -10 Hawaii-Aleutian(No DST)]",
            "Australia/Adelaide"   => "[UTC +9:30 Adelaide]",
            "Australia/Brisbane"   => "[UTC +10 Brisbane]",
            "Australia/Broken_Hill"=> "[UTC +9:30 Broken Hill]",
            "Australia/Currie"     => "[UTC +10 Currie]",
            "Australia/Darwin"     => "[UTC +9:30 Darwin]",
            "Australia/Eucla"      => "[UTC +8:45 Eucla]",
            "Australia/Hobart"     => "[UTC +10 Hobart]",
            "Australia/Lindeman"   => "[UTC +10 Lindeman]",
            "Australia/Lord_Howe"  => "[UTC +10:30 Lord Howe]",
            "Australia/Melbourne"  => "[UTC +10 Melbourne]",
            "Australia/Perth"      => "[UTC +8 Perth]",
            "Australia/Sydney"     => "[UTC +10 Sydney]",
            "Europe/Amsterdam"     => "[UTC +1 Amsterdam]",
            "Europe/Andorra"       => "[UTC +1 Andorra]",
            "Europe/Athens"        => "[UTC +2 Athens]",
            "Europe/Belgrade"      => "[UTC +1 Belgrade]",
            "Europe/Berlin"        => "[UTC +1 Berlin]",
            "Europe/Bratislava"    => "[UTC +1 Bratislava]",
            "Europe/Brussels"      => "[UTC +1 Brussels]",
            "Europe/Bucharest"     => "[UTC +2 Bucharest]",
            "Europe/Budapest"      => "[UTC +1 Budapest]",
            "Europe/Busingen"      => "[UTC +1 Busingen]",
            "Europe/Chisinau"      => "[UTC +2 Chisinau]",
            "Europe/Copenhagen"    => "[UTC +1 Copenhagen]",
            "Europe/Dublin"        => "[UTC Dublin]",
            "Europe/Gibraltar"     => "[UTC +1 Gibraltar]",
            "Europe/Guernsey"      => "[UTC Guernsey]",
            "Europe/Helsinki"      => "[UTC +2 Helsinki]",
            "Europe/Isle_of_Man"   => "[UTC Isle of Man]",
            "Europe/Istanbul"      => "[UTC +3 Istanbul]",
            "Europe/Jersey"        => "[UTC Jersey]",
            "Europe/Kaliningrad"   => "[UTC +2 Kaliningrad]",
            "Europe/Kiev"          => "[UTC +2 Kiev]",
            "Europe/Lisbon"        => "[UTC Lisbon]",
            "Europe/Ljubljana"     => "[UTC +1 Ljubljana]",
            "Europe/London"        => "[UTC London]",
            "Europe/Luxembourg"    => "[UTC +1 Luxembourg]",
            "Europe/Madrid"        => "[UTC +1 Madrid]",
            "Europe/Malta"         => "[UTC +1 Malta]",
            "Europe/Mariehamn"     => "[UTC +2 Mariehamn]",
            "Europe/Minsk"         => "[UTC +3 Minsk]",
            "Europe/Monaco"        => "[UTC +1 Monaco]",
            "Europe/Moscow"        => "[UTC +3 Moscow]",
            "Europe/Oslo"          => "[UTC +1 Oslo]",
            "Europe/Paris"         => "[UTC +1 Paris]",
            "Europe/Podgorica"     => "[UTC +1 Podgorica]",
            "Europe/Prague"        => "[UTC +1 Prague]",
            "Europe/Riga"          => "[UTC +2 Riga]",
            "Europe/Rome"          => "[UTC +1 Rome]",
            "Europe/Samara"        => "[UTC +4 Samara]",
            "Europe/San_Marino"    => "[UTC +1 San Marino]",
            "Europe/Sarajevo"      => "[UTC +1 Sarajevo]",
            "Europe/Simferopol"    => "[UTC +3 Simferopol]",
            "Europe/Skopje"        => "[UTC +1 Skopje]",
            "Europe/Sofia"         => "[UTC +2 Sofia]",
            "Europe/Stockholm"     => "[UTC +1 Stockholm]",
            "Europe/Tallinn"       => "[UTC +2 Tallinn]",
            "Europe/Tirane"        => "[UTC +1 Tirane]",
            "Europe/Uzhgorod"      => "[UTC +2 Uzhgorod]",
            "Europe/Vaduz"         => "[UTC +1 Vaduz]",
            "Europe/Vatican"       => "[UTC +1 Vatican]",
            "Europe/Vienna"        => "[UTC +1 Vienna]",
            "Europe/Vilnius"       => "[UTC +2 Vilnius]",
            "Europe/Volgograd"     => "[UTC +3 Volgograd]",
            "Europe/Warsaw"        => "[UTC +1 Warsaw]",
            "Europe/Zagreb"        => "[UTC +1 Zagreb]",
            "Europe/Zaporozhye"    => "[UTC +2 Zaporozhye]",
            "Europe/Zurich"        => "[UTC +1 Zurich]"
        ];

       if(array_key_exists($timeZone,$timeZonesStack)){
           return $timeZonesStack[$timeZone];
       }

    }
}