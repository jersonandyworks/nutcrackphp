<?php
/**
 * Created by PhpStorm.
 * User: andybarrios
 * Date: 03/12/2016
 * Time: 2:01 PM
 */
namespace Nutcrack\Controllers;
use Slim\Views\Twig as View;
use Pusher;
class PusherController extends BaseController
{


    public function index($request,$response,$args){

       return $this->view->render($response,'pusher.html');

    }

    public function subscribe($request,$response,$args){
        $options = array(
            'cluster' => 'ap1',
            'encrypted' => true
          );
          $pusher = new Pusher\Pusher(
            '9d090aad583f458f0827',
            '24ac71ff91530da02f9c',
            '372307',
            $options
          );

          $data['message'] = 'hello world';
          $pusher->trigger('my-channel', 'my-event', $data);
    }
}