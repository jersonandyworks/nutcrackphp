<?php

namespace Nutcrack\Controllers;
use Carbon\Carbon as Carbon;
use Nutcrack\Models\ScanResult as ScanResult;
use Slim\Views\Twig as View;
use Illuminate\Database\Capsule\Manager as Capsule;
class ScanController extends BaseController{
	public function index($request,$response,$args){
		$json['data'] = [];

		switch($_REQUEST['action'])
        {
            case 'noscan':
                $json['data'][] = ['test'=>'1','man'=>'1','here'=> \Carbon\Carbon::yesterday()];
                break;
            case 'failedscans':
            	$db = new Capsule;

                if(isset($_REQUEST['site_id'])){
                    $site_id = $_REQUEST['site_id'];
				    
                    $failedScans = ScanResult::failedScansBySiteID($site_id);
					$errorsResult = ScanResult::errorResults($site_id);
															
                }else{
                	$failedScans = ScanResult::allFailedScans();
                }
              
                foreach($failedScans as $f){

                    $json['data'][$f->siteID]['fields'][] = [
	                    	'scanResultID' => $f->scanResultID,
	                        'siteID'       => $f->siteID,
	                        'cloneScanID'  => $f->cloneScanID,
	                        'reportID'     => $f->reportID,
	                        'status'       => $f->status,
	                        'progress'     => $f->progress,
	                        'severity'     => $f->severity,
	                        'serious'      => $f->serious,
	                        'high'         => $f->high,
	                        'medium'       => $f->medium,
	                        'low'          => $f->low,
	                        'info'         => $f->info,
	                        'created'      => $f->created,
	                        'finished'     => $f->finished,
                            'domain'       => $f->trustsite->FullSite,
                            'businessName' => $f->trustsite->BusinessName
                    ];
                     $json['data'][$f->siteID]['errors'] = [
                        'total_low' => intval((isset($errorsResult) && $errorsResult->low_errors != null)? $errorsResult->low_errors : ScanResult::calculateLow($f->siteID)),
                        'total_medium' => intval((isset($errorsResult) && $errorsResult->medium_errors != null)? $errorsResult->medium_errors : ScanResult::calculateMedium($f->siteID)),
                        'total_high' => intval( (isset($errorsResult) && $errorsResult->high_errors != null)? $errorsResult->high_errors : ScanResult::calculateHigh($f->siteID)),
                     ];
                   
                     
                }//end of loop

              
                break;
            case 'deactivatedip':
                break;
            case 'inactiveusers':
                break;
            case 'userattempts':
                break;
            case 'shortscan':
                break;
        }

		return $response->withJson($json,201);
	}
    public function multiplescans($req,$res,$args){
        return "Multiple scans";
    }
}