<?php
/**
 * Created by PhpStorm.
 * User: andybarrios
 * Date: 03/12/2016
 * Time: 2:34 PM
 */

namespace Nutcrack\Controllers;
use Illuminate\Database\Query\Builder;

class BookingController
{
    private $view;
    private $logger;
    protected $table;

    public function __construct(
        Twig $view,
        Builder $table
    ) {
        $this->view = $view;
        $this->logger = $logger;
        $this->table = $table;
    }

    public function index(){
       return $this->table->get();
    }
}