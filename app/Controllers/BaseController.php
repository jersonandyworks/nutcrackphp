<?php
/**
 * Created by PhpStorm.
 * User: andybarrios
 * Date: 03/12/2016
 * Time: 6:06 PM
 */

namespace Nutcrack\Controllers;
class BaseController
{
    protected $container;
    private $response;
    public function __construct($container){
        $this->container = $container; 
    }

    public static function redirect($response,$url){
        return $response->withStatus(302)->withHeader('Location',$url);
    }

    public function __get($property){
        if($this->container->{$property}){
            return $this->container->{$property};
        }
    }
}