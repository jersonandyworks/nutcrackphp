<?php
/**
 * Created by PhpStorm.
 * User: andybarrios
 * Date: 03/12/2016
 * Time: 2:01 PM
 */
namespace Nutcrack\Controllers;
use RedBeanPHP\Facade as R;
use Slim\Views\Twig as View;
use Nutcrack\Models\User as User;
use Nutcrack\Models\ScanResult as Scan;
use Nutcrack\Models\TrustSites as TrustSites;
use Nutcrack\Models\ScanMigration as ScanMigration;
use Cartalyst\Sentry\Facades\Native\Sentry as Auth;
use Nutcrack\Models\Post as Post;
use Nutcrack\Models\BeyondSecurityMigration as BeyondSecurityMigration;
use Nutcrack\Models\StripeCustomerBilling as StripeCustomerBilling;
class UserController extends BaseController
{
    /*public function __construct(){
        if( ! Auth::check() ){
            return header('Location: /login',true,302);
        }
    }*/

    public function index(){
        // $ts = TrustSites::chunk(100, function($sites){
        //     foreach($sites as $site){
        //         echo $site->FullSite;
        //     }
        // });

        $ts = ScanMigration::where('siteID','45')->first();
        $inBSMigration = BeyondSecurityMigration::where('siteId', $ts->siteID)->first();
        echo $inBSMigration->site->FullSite;

        BeyondSecurityMigration::create([
            "siteId" =>  $ts->siteID,
            "status" => "pending"
        ]);


    }
    public function posts($request,$response,$args){

        $post = Post::find(3);
        $scans = Scan::chunk(100,function($scanss){
            foreach($scanss as $scan){
                echo $scan->reportID."<br>";
            }
        });

    }
    public function create($request,$response,$args){
        User::add();
    }

    public function checkUser(){

    }

    public function form($request,$response,$args){
        return $this->view->render($response,'form.html');
    }
    public function checking($request,$response,$args){
        $input = $_POST;
        Post::create(['title'=>'hello','slug'=> 'test' , 'content'=>$input['content']]);
        $this->flash->addMessage('test','Submitted!');
        //redirect
        return self::redirect($response,'/');
    }

    public function lists($request, $response, $args){
        Auth::createUser(array(
            'email'     => 'kiyo@hotmail.com',
            'password'  => 'ihavecontrol132!@',
            'activated' => true,
        ));
        $users = User::all();
        $this->view->render($response,'users.html',['users'=>$users]);
    }

    public function scans($request,$response,$args){
        $scans = Scan::where([
            ['siteID',1229],
            ['reportID','!=',null],
        ])->get();
        $this->view->render($response,'users.html',['scans'=>$scans]);
    }

    public function createUser($request,$response,$arg){
        $username = $arg['username'];
        $password = $arg['password'];
        $user = R::dispense('users');
        $user->username = $username;
        $user->password = $password;
        R::store($user);
        return self::redirect($response,'/');
    }
    public function paypalIPN($req,$res,$args){
      
    }
    public function success($req,$res,$args){
         return $this->view->render($res,'test.html');
//        var_dump($_POST);
    }
    public function billing($req,$res,$args){
         return $this->view->render($res,'billing.html');
//        var_dump($_POST);
    }
    public function processStripeBilling($req,$res,$args){
        $stripe = array(
          "secret_key"      => "sk_test_yU6Zfi7fbpcylZMBxdqJqXmB",
          "publishable_key" => "pk_test_pfjQn73GoOmgP35YgvnONhJT"
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        var_dump($_POST);
        $token  = $_POST['stripeToken'];

          $customer = \Stripe\Customer::create(array(
              'email' => 'customer@example.com',
              'address' => $_POST['address'],
              'source'  => $token
          ));

          $charge = \Stripe\Charge::create(array(
                  'customer' => $customer->id,
                  'amount'   => 5000,
                  'currency' => 'usd'
              ));


              echo '<h1>Successfully charged $50.00!</h1>';
    }
}