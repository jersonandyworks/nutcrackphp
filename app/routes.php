<?php

use Nutcrack\Models\ScanResult as ScanResult;
use Nutcrack\Models\Company as Company;
use Nutcrack\Models\CompanyMember as CompanyMember;
use Nutcrack\Models\Cancellations as Cancellation;
use Illuminate\Database\Capsule\Manager as DB;
use Nutcrack\Plugins\BoothReservations\Booth as Booth;
use Nutcrack\Models\Cart as Cart;
use Illuminate\Support\Collection as Collection;
use Nutcrack\Helpers\String as StringHelper;


/*Use for acception HTTP requests*/
$app->get('/pusher','PusherController:index');
$app->get('/subscribe','PusherController:subscribe');

$app->get('/','UserController:index');
$app->get('/success','UserController:success');
$app->get('/multiplescans','ScanController:multiplescans')->setName('multilplescans');
$app->get('/newdevices','TestController:newdevices')->setName('newdevices');
$app->get('/booth',function($req,$res,$arg){
    if( isset($_COOKIE['cart']) ){
        $stringRand = $_COOKIE['cart'];
        echo "set!";
    }else{
        $stringRand = StringHelper::random_str(12);
    }
    Cart::create(['name'=>StringHelper::random_str(4),"price"=>StringHelper::random_str(3,"0123456789"),"session_id"=>$stringRand]);
    setcookie("cart",$stringRand);
//    $carts = Cart::whereRaw('created_at >= date_sub(NOW(),INTERVAL 2 HOUR)')->get();
//    $carts = Cart::all();

//    return $res->withJson($carts);
});

$app->get('/schemes',function($req,$res,$arg){
    $carts = Cart::whereRaw('created_at < NOW() - INTERVAL 2 HOUR')->get();
    Cart::whereRaw('created_at< NOW() - INTERVAL 2 HOUR')->delete();
});

$app->get('/test',function($req,$res){
    /*$string = 'cup';
    $name = 'coffee';
    $str = 'This is a $string with my $name in it.';

    echo $str."<hr>";
    $replaced = str_replace(['$string','$name'], [$string,$name], $str);
    echo $replaced;*/

    echo date('Y-m-d h:i:s');  
});




$app->get('/billing','UserController:billing');
$app->get('/partners','PartnersController:index');
$app->get('/partners/remove/{id}','PartnersController:removeProduct');
$app->get('/partners/product/{id}','PartnersController:getProductInfo');
$app->get('/products','PartnersController:getProducts');
$app->get('/server','PartnersController:server');
$app->get('/tester/{id}','PartnersController:test');
$app->get('/tester/remove/{id}','PartnersController:remove');
$app->post('/processCreateProduct','PartnersController:addProduct');
$app->post('/processStripeBilling','BillingController:processBill');
$app->get('/decode',function($req,$res,$arg){

/*    $db = new DB();
    $template = $db->table('EmailTemplate')
        ->select('content')
        ->first();



    $content = $template->content;
    $stepOne = preg_replace(['/{/','/}/'],['$',''],$content);
    echo $stepOne;

    $matches = preg_match(['/{/','/}/'],$m,$content);*/

    $Email = "barriosjerson@gmail.com";
    $UserName = "ravenfox";
    $FullName = "johndoe";

    $content ="{FullName} and {UserName} {Email}</p>";
    preg_match_all('/{(.*?)}/', $content, $matches);
    $matchedValues = array_map('strval',$matches[1]);

    $stepOne = preg_replace(['/{/','/}/'],['$',''],$content);
//    $stepTwo= preg_replace('/}/','',$steecho $replaced;
//    var_dump($matchedValues);
});

$app->get('/stripe',function($req,$res){
    $member = \Nutcrack\Models\Member::where('Email','aubreybarker@gmail.com')->value('MemberId');
    echo $member;
});
