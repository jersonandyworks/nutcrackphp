<?php
/**
 * Created by Jerson Andy Barrios.
 * User: NerdGeek
 * Date: 5/13/2017
 * Time: 7:47 PM
 */

namespace Nutcrack\Plugins\BoothReservations;
use Illuminate\Support\Collection as Collection;

class Booth
{
    private $booth;
    public function __construct(){

    }

    public function insert($name,$price,$note="booth insert") {
        // Create new booth collection
        // if there are previous collection do
        $this->booth = Collection::make(['name' => $name, 'price' => $price, 'note' => $note]);
        if($this->booth->count() > 1){
            $this->booth = $this->booth->pop(['name' => $name, 'price' => $price, 'note' => $note]);
        }else{
            // push/add new collection to previous collection

        }
        $this->setCollection($this->booth);
    }

    private function setCollection($collection){
        return $collection->all();['
        ']
    }

    public function getCollection(){
        return $this->booth;
    }
}