<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class CloneException extends Eloquent{
	protected $table = 'CloneException';
	protected $primaryKey = 'cloneExceptionID';

}