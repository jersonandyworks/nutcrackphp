<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class CompanyScanning extends Eloquent{
	protected $table = 'CompanyScanning';
	protected $primaryKey = 'companyScanningID';

	public function company(){
		return $this->belongsTo('Nutcrack\Models\Company','companyID');
	}

	public static function subscriptions(){

        $results = DB::table('CompanyScanning')
                       ->select(DB::raw(
                           'CompanyScanning.companyID,CompanyScanning.dailyUsed,
                            CompanyScanning.dailyAvailable,
                            CompanyScanning.weeklyUsed,
                            CompanyScanning.weeklyAvailable,
                            CompanyScanning.monthlyUsed,
                            CompanyScanning.monthlyAvailable,
                            CompanyScanning.quarterlyUsed,
                            CompanyScanning.quarterlyAvailable,
                            CompanyScanning.manualUsed,CompanyScanning.manualAvailable,
                            Company.companyName,
                            count(CompanySite.companyID) as totalSite,
                            (CompanyScanning.dailyAvailable + weeklyAvailable 
                             + CompanyScanning.monthlyAvailable 
                             + CompanyScanning.quarterlyAvailable 
                             + manualAvailable) AS availableScans
                           '))
                        ->join('Company',function($join){
                            $join->on('CompanyScanning.companyID','=','Company.companyID');
                        })
                        ->join('CompanySite',function($join){
                            $join->on('Company.companyID','=','CompanySite.companyID');
                        })
                        ->whereRaw('CompanyScanning.dailyUsed > CompanyScanning.dailyAvailable
                                   OR (CompanyScanning.weeklyUsed > CompanyScanning.weeklyAvailable)
                                   OR (CompanyScanning.monthlyUsed > CompanyScanning.monthlyAvailable)
                                   OR (CompanyScanning.quarterlyUsed > CompanyScanning.quarterlyAvailable)
                                   OR (CompanyScanning.manualUsed > CompanyScanning.manualAvailable)
                                   AND CompanyScanning.companyID != 0
                                  ')
                        ->havingRaw('totalSite >= availableScans')
                        ->groupBy('Company.companyID')
                       ->get();

        return $results;
	}
    public static function getTotalDomainByCompanyID($companyID){
        return CompanySite::where('companyID',$companyID)->count('companyID');

    }
}