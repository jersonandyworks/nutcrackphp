<?php
/**
 * Created by PhpStorm.
 * User: nerdgeek
 * Date: 12/11/16
 * Time: 7:02 PM
 */

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class StripeAccount extends Eloquent{
    protected $table = 'StripeAccount';
    protected $primaryKey = 'stripeAccountID';
    public $timestamps = false;
    protected $fillable = ['customerID','memberID',
                           'staffID','parentMemberID',
                           'emailAddress','created'
                         ];
}