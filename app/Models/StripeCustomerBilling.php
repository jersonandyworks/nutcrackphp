<?php
/**
 * Created by PhpStorm.
 * User: nerdgeek
 * Date: 12/11/16
 * Time: 7:02 PM
 */

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class StripeCustomerBilling extends Eloquent{
    protected $table = 'StripeCustomerBilling';
    protected $primaryKey = 'stripeCustomerBillingID';
    public $timestamps = false;
    protected $fillable = ['stripeCustomerBillingID','stripeAccountID',
                         'stripeSubscriptionID','staffID',
                         'stripePlanID','status',
                         'firstChargeDate','intervalType',
                         'totalCharges','chargesLeft',
                         'notes','created'
                         ] ;
}