<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Company extends Eloquent{
	protected $table = 'Company';
	protected $primaryKey = 'companyID';
	public function scannings(){
		return $this->hasMany('Nutcrack\Models\CompanyScanning','companyID');
	}
	public function scopeGetAllByPartnerID($query,$partnerID){
        return $query->where([
            ['companyPartnerID',$partnerID],
        ])
            ->orderBy('companyPartnerID','asc')
            ->get();
    }
    public function scopeGetNoPCI($query){
    	$results = DB::table('Company')
            ->select(DB::raw('Company.companyID,Company.companyName,IFNULL(SUM(ScanResult.serious),0) AS serious,SUM(ScanResult.high) AS high,SUM(ScanResult.medium) AS medium,SUM(ScanResult.low) AS low,ScanResult.status,SUM(high + medium + low) AS total'))
            ->join('CompanySite',function($join){
                $join->on('Company.companyID','=','CompanySite.companyID')

                    ->where('Company.companyPartnerID','=',2);
            })
             ->join('ScanResult',function($join){
                $join->on('CompanySite.SiteId','=','ScanResult.siteID')
                    ->where('ScanResult.status','=','fail');

            })
            ->join('TrustSites',function($join){
                $join->on('TrustSites.SiteId','=','ScanResult.siteID');
            })
           
            ->groupBy('Company.companyID')
            /*->orderBy('ScanResult.finished')
            ->orderBy('ScanResult.high')*/
            ->get();
        
        return $results;

    }
    public static function getTrialMerchants()
    {
        $db = new DB;

        $rawSQL =  "SELECT 
                        Company.companyID,
                        Company.companyName,
                        Company.accountType,
                        Company.companyPartnerID,
                        Company.resellerCompanyID,
                        CompanySite.siteID,
                        TrustSites.FullSite,
                        TrustSites.BusinessName,
                        SUM(ScanResult.serious) serious,
                        SUM(ScanResult.high) high,
                        SUM(ScanResult.medium) medium,
                        SUM(ScanResult.low) low,
                        SUM(ScanResult.info) info
                    FROM Company
                    JOIN CompanySite ON Company.companyID = CompanySite.companyID
                    JOIN TrustSites ON TrustSites.SiteId = CompanySite.siteID
                    JOIN (
                        SELECT
                            ScanResult.serious,
                            ScanResult.high,
                            ScanResult.medium,
                            ScanResult.low,
                            ScanResult.info,
                            ScanResult.siteID,
                            ScanResult.status
                        FROM 
                         ScanResult
                    ) ScanResult ON ScanResult.siteID = TrustSites.SiteId
                    WHERE Company.companyPartnerID = 2
                    AND Company.accountType = 'trial'
                    
                    GROUP BY companyID
                    ORDER BY serious DESC, high DESC, medium DESC, low DESC, info DESC";

        return $db::select($rawSQL);
    }
    public function scopeGetNewDevices($query){
        $results = DB::table('Company')
            ->select(
                DB::raw('CompanySite.siteID,
                         Company.companyID,
                         Company.companyName,
                         Company.companyPartnerID,
                         Company.created,
                         TrustSites.Dscan,
                         TrustSites.Wscan,
                         TrustSites.Mscan,
                         TrustSites.Qscan,
                         TrustSites.Oscan,
                         TrustSites.FullSite
                         ')
                )
            ->join('CompanySite',function($join){
                $join->on('Company.companyID','=','CompanySite.companyID');
            })
            ->join('TrustSites',function($join){
                $join->on('TrustSites.SiteId','=','CompanySite.siteID');
            })
            ->where('Company.created','!=','0000-00-00 00:00:00')
            ->whereRaw('TrustSites.Created BETWEEN date_sub(now(),INTERVAL 2 week) AND now()')
            ->orderBy('Company.created','DESC')
            ->get();
        
        return $results;
    }
    public function scopeGetNewMerchants($query, $companyPartnerID, $month=1)
    {
        return $query->where([
            ['companyPartnerID',$companyPartnerID],
            ['created','>=',\Carbon\Carbon::now()->subMonth($month)],
            ['accountType','regular']
        ])->orderBy('created','desc')->get();
    }
    public function scopeGetReseller($query,$companyID){
        return $query
            ->where('companyID',$companyID)->value('companyName');
        }
	public function scopeGetSelectedUserType($query,$companyID){
		return $query->where('companyID',$companyID)->first();
	}
    public function scopeGetMultipleScans($query,$partnerID=2){
        $results = DB::table('Company')
            ->select(
                DB::raw('ScanResult.scanResultID,
                         Company.companyID,
                         Company.companyName,
                         Company.companyPartnerID,
                         ScanResult.scanResultID,
                         ScanResult.siteID,
                         IFNULL(ScanResult.serious,0) AS serious,
                         ScanResult.high,
                         ScanResult.medium,
                         ScanResult.low,
                         ScanResult.info,
                         high + medium + low + info AS total,
                         TrustSites.Dscan,
                         TrustSites.Wscan,
                         TrustSites.Mscan,
                         TrustSites.Qscan,
                         TrustSites.Oscan,
                         TrustSites.FullSite,
                         Company.created,
                         COUNT(ScanResult.siteID) as scanCount,
                         ScanResult.created as lastScan
                         ')                  
                
                )
            ->join('CompanySite',function($join){
                $join->on('Company.companyID','=','CompanySite.companyID')

                    ->where('Company.companyPartnerID','=',2);
            })
             ->join('ScanResult',function($join){
                $join->on('CompanySite.SiteId','=','ScanResult.siteID');
            })
            ->join('TrustSites',function($join){
                $join->on('TrustSites.SiteId','=','ScanResult.siteID');
            })
            ->where('Company.created','!=','0000-00-00 00:00:00')
            ->whereRaw('ScanResult.created > DATE_SUB(NOW(),INTERVAL 24 HOUR)')
            ->orderBy('ScanResult.siteID')
            ->orderBy('ScanResult.created','DESC')
            ->groupBy('ScanResult.siteID')
            ->get();
        
        return $results;
    }
    public function getCancelledSitesByPartnerID($partnerID=2){
        $results = DB::table('Company')
                   ->select(
                       DB::raw('
                                Cancellations.Id,
                                Company.companyName,
                                Company.companyPartnerID,
                                Cancellations.SiteName,
                                TrustSites.Dscan,
                                TrustSites.Wscan,
                                TrustSites.Mscan,
                                TrustSites.Qscan,
                                TrustSites.Oscan,
                                TrustSites.FullSite,
                                MAX(Cancellations.Date) as cancelledDate,
                                ScanResult.created as lastScan
                   '))
                   ->join('CompanySite',function($join) use ($partnerID){
                       $join->on('Company.companyID','=','CompanySite.companyID')
                            ->where('Company.companyPartnerID','=',$partnerID);
                   })
                    ->join('ScanResult',function($join){
                        $join->on('CompanySite.SiteId','=','ScanResult.siteID');
                    })
                   ->join('TrustSites',function($join){
                       $join->on('CompanySite.siteID','=','TrustSites.SiteId');
                   })
                   ->join('Cancellations',function($join){
                       $join->on('TrustSites.FullSite','=','Cancellations.SiteName');
                   })
                   ->whereRaw('DATE_FORMAT(ScanResult.created,"%Y-%m-%d") > Cancellations.Date')
                   ->groupBy('TrustSites.FullSite')
                   ->orderBy('ScanResult.created','DESC')
                   ->get();
        return $results;
    }
    public function getScanPendingStatus(){
        $results = DB::table('Company')
            ->select(
                DB::raw('
                                ScanResult.siteID,
                                Company.companyName,
                                Company.companyPartnerID,
                                TrustSites.FullSite,
                                ScanResult.status,
                                MAX(ScanResult.created) AS lastScan
                   '))
            ->join('CompanySite',function($join){
                $join->on('Company.companyID','=','CompanySite.companyID');
            })
            ->join('ScanResult',function($join){
                $join->on('CompanySite.SiteId','=','ScanResult.siteID');
            })
            ->join('TrustSites',function($join){
                $join->on('CompanySite.siteID','=','TrustSites.SiteId');
            })
            ->where('ScanResult.status','stuck')
            ->groupBy('TrustSites.FullSite')
            ->groupBy('ScanResult.siteID')
            ->orderBy('ScanResult.created','DESC')
            ->get();
        return $results;
    }
    public function scopeGetCompany($query,$companyID)
    {
        return $query->where('companyID', $companyID)->first();
    }
}