<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class ScanMigration extends Eloquent{
	protected $table = 'ScanMigration';
    protected $primaryKey = 'scanMigrationID';   
    protected $guarded = [];  
}