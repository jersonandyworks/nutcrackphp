<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class PartnerSite extends Eloquent{
	protected $table = 'PartnerSite';
	protected $primaryKey = 'partnerSiteID';

	public function partner(){
		return $this->belongsTo('Nutcrack\Models\CompanyPartner','companyPartnerID');
	}

	public function site(){
		return $this->belongsTo('Nutcrack\Models\TrustSite','siteID');
	}
}