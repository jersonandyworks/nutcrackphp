<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class TrustSites extends Eloquent{
	protected $table = 'TrustSites';
    protected $primaryKey = 'SiteId';

    public function scans(){
    	return $this->hasMany('Nutcrack\Models\ScanResult','siteID');
    }

    public function bsinfos(){
        return $this->hasMany('Nutcrack\Models\BeyondSecurityMigration','SiteId');
    }

    public function companymembers(){
    	return $this->hasMany('Nutcrack\Models\CompanyMember','memberID');
    }
    public function partners(){
    	return $this->hasMany('Nutcrack\Models\CompanyMember','siteID');
    }

    public static function countTotalSitesByCompanyID(){

    }
}