<?php
/**
 * Created by Jerson Andy Barrios.
 * User: NerdGeek
 * Date: 5/14/2017
 * Time: 7:17 AM
 */

namespace Nutcrack\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
class Cart extends Eloquent
{
    protected $fillable = ['name','price','created_at','updated_at','session_id'];
}