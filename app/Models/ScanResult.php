<?php
/**
 * Created by PhpStorm.
 * User: nerdgeek
 * Date: 12/11/16
 * Time: 7:02 PM
 */

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class ScanResult extends Eloquent{
    protected $table = 'ScanResult';
    protected $primaryKey = 'scanResultID';
    public function trustsite(){
    	return $this->belongsTo('Nutcrack\Models\TrustSite','siteID');
    }
    public static function scopeFailedScansBySiteID($query,$site_id){
    	return $query->where([ ['siteID',$site_id],['status','fail'] ])
											   ->orderBy('created','desc')
											   ->get();
    }
    public static function scopeGetTrustSiteInfo($query,$site_id){
        return $query->where('siteID',$site_id)->first();
    }
    public static function scopeAllFailedScans($query){
    	return $query->where('status','fail')->groupBy('siteID')->orderBy('created','desc')->get();
    }
    public static function errorResults($site_id){

    	$db = new Capsule;
    	return $db->table('ScanResult')
				   ->select(Capsule::raw('sum(low) as low_errors, sum(medium) as medium_errors, sum(high) as high_errors'))
				   ->where([ ['siteID',$site_id],['status','fail'] ])
				   ->groupBy('siteID')
				   ->orderBy('created','desc')
				   ->first();
    }
    public static function calculateLow($site_id){
    	return self::where([['status','fail'],['siteID',$site_id]])->sum('low');
    }
    public static function calculateMedium($site_id){
    	return self::where([['status','fail'],['siteID',$site_id]])->sum('medium');
    }
    public static function calculateHigh($site_id){
    	return self::where([['status','fail'],['siteID',$site_id]])->sum('high');
    }
    public static function calculateTimeDifference($site_id){
    	$db = new Capsule;
    	return $db->table('ScanResult')->select(
                            Capsule::raw(
                                'TIMESTAMPDIFF(MINUTE,created,finished) as diff_in_minutes, 
                                TIMESTAMPDIFF(HOUR,created,finished) as diff_in_hours'
                            ))
                            ->where(
                                [
                                    ['siteID', $site_id], // 10769
                                    ['created', '!=', null],
                                    ['finished', '!=', null]
                                ]
                            )->first();
    }
    public static function multipleScans(){

    }
    public function scopeLastScan($query,$siteID){
       return $query->where('siteID',$siteID)
            ->orderBy('created')
            ->first();
    }
}