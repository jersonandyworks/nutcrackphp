<?php
/**
 * Created by Jerson Andy Barrios.
 * User: NerdGeek
 * Date: 5/9/2017
 * Time: 10:47 PM
 */

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class Post extends Eloquent
{
    protected $fillable = ['title','slug','content'];
    public $timestamps = false;
}