<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class CompanyMember extends Eloquent{
	protected $table = 'CompanyMember';
	protected $primaryKey = 'companyMemberID';

	public function trustite(){
		return $this->belongsTo('Nutcrack\Models\TrustSite','memberID');
	}

}