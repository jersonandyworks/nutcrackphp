<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class BeyondSecurityMigration extends Eloquent{
	protected $table = 'BeyondSecurityMigration';
    protected $primaryKey = 'bsMigrationId';
    protected $guarded = []; 

    public function info(){
    	return $this->belongsTo('Nutcrack\Models\BeyondSecurityInfo','beyondSecurityId');
    }
 
    public function site(){
    	return $this->belongsTo('Nutcrack\Models\TrustSites','siteId');
    }

}