<?php
/**
 * Created by PhpStorm.
 * User: andybarrios
 * Date: 24/06/2017
 * Time: 5:53 PM
 */

namespace Nutcrack\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Member extends Eloquent
{
    protected $table = 'Members';
    protected $primaryKey = 'MemberId';
}
