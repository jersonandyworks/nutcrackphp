<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class BeyondSecurityInfo extends Eloquent{
	protected $table = 'BeyondSecurityInfo';
    protected $primaryKey = 'beyondSecurityId';

    public function migrations(){
    	return $this->hasMany('Nutcrack\Models\BeyondSecurityMigration','beyondSecurityId');
    }

}