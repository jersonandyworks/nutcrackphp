<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class CompanySite extends Eloquent{
	protected $table = 'CompanySite';
	protected $primaryKey = 'companySiteID';

    public static function getTotalDomainByCompanyID($companyID){
        return self::where('companyID',$companyID)->count('companyID');

    }
    
}