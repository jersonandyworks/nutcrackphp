<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class PartnerProduct extends Eloquent{
	protected $table = 'PartnerProduct';
    protected $primaryKey = 'partnerProductID';
    public $timestamps = false;
    protected $fillable = ['partnerProductID','memberID','productName',
                           'productDescription','planGeneratedID','termType',
                           'holdDays','productType','scanType','status','created'];
}