<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;

class Cancellations extends Eloquent{
	protected $table = 'Cancellations';
    protected $primaryKey = 'Id';

    public function siteNames(){
        return  DB::table('Cancellations')
                ->select(DB::raw('Id,SiteName,MAX(Date) as date'))
                ->where('SiteName','!=','')
                ->groupBy('SiteName')
                ->orderBy('Date','DESC')
                ->pluck('SiteName');
        //self::where('SiteName','!=','')->orderBy('Date','desc')->groupBy('SiteName')->get();
    }
}