<?php

namespace Nutcrack\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class CompanyPartner extends Eloquent{
	protected $table = 'CompanyPartner';
	protected $primaryKey = 'CompanyPartnerID';

	public function resellers(){
		return $this->hasMany('Nutcrack\Models\PartnerSite','companyPartnerID');
	}
}