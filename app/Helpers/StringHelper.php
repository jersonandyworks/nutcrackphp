<?php
/**
 * Created by Jerson Andy Barrios.
 * User: NerdGeek
 * Date: 5/14/2017
 * Time: 7:32 AM
 */

namespace Nutcrack\Helpers;
use Stripe\Error\InvalidRequest;
use Nutcrack\Models\StripeCustomerBilling as StripeCustomerBilling;
use Nutcrack\Models\StripeAccount as StripeAccount;
class StringHelper
{
    private $str;
    private $max;
    public function  __construct(){
        $this->str = $str;
        $this->max = $str;
    }
    public static function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()~')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }
    public static function convertCurlyToVariable($content,$obj){
        $string = "";
        $convertedVariable = explode(" ", $content);
        foreach($convertedVariable as $c){
            if(strstr($c,"{")){
                $var = str_replace(["{","}","{{","}}"],"", $c);
                $string .=  $obj->$var." ";
                continue;
            }
            $string .= $c." ";
        }
        return $string;
    }

    public function planGenerator($data){
        $stripe = array(
          "secret_key"      => "sk_test_yU6Zfi7fbpcylZMBxdqJqXmB",
          "publishable_key" => "pk_test_pfjQn73GoOmgP35YgvnONhJT"
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $stripePlanID = '';
        $billingInterval = "weekly"; //$data['intervalType'];
        $billingTimes = 10; //$data['totalCharges'];
        $billingAmount= 10; //$data['billingAmount'];

        if ($billingInterval != "onetime")
        {
            
            $stripePlanID = "plan-" . $billingInterval . "-" . $billingTimes . "-" .intval(($billingAmount * 100));
            try{
                $stripePlan = \Stripe\Plan::retrieve($stripePlanID);
             }catch (InvalidRequest $e){
                $plan = \Stripe\Plan::create(array(
                          "name" => $stripePlanID,
                          "id" => $stripePlanID,
                          "interval" => "week", //$billingInterval,
                          "currency" => "usd",
                          "amount" => intval(($billingAmount * 100)),
                        ));
             }
             $customer = \Stripe\Customer::create(array(
                          "email" => 'test@gmail.com',//$data['email'],
                          "source" => $data['stripeToken']
                        ));

                $subscription = \Stripe\Subscription::create(array(
                      "customer" => $customer->id,
                      "plan" => $stripePlanID,
                    ));
             echo "customerID: ".$customer->id;
             echo "subscription: ".$subscription->id;
             // var_dump($subscription);
             /*$stripeAccount = StripeAccount::create(['customerID'=>$customer->id,'email'=>'test@gmail.com','created'=>date('Y-m-d h:is')]);
             StripeCustomerBilling::create(['stripeAccountID'=>$stripeAccount->stripeAccountID,
                                         'stripeSubscriptionID'=>$subscription->id,
                                         'staffID'=>'',
                                         'stripePlanID'=>$stripePlanID,
                                         'status'=>'active',
                                         'firstChargeDate'=>$data['firstPayment'],
                                         'intervalType'=>$data['intervalType'],
                                         'totalCharges'=>$data['totalCharges'],
                                         'chargesLeft'=>0,
                                         'notes'=>''
                                         ]);*/
        }else{
            $customer = \Stripe\Customer::create(array(
              'email' => 'test@gmail.com', //$data['email'],
              'source'  => $data['stripeToken']
          ));

          $charge = \Stripe\Charge::create(array(
              'customer' => $customer->id,
              'amount'   => intval((1000 * 100)),
              'currency' => 'usd'
          ));

          /*$stripeAccount = StripeAccount::create(['customerID'=>$customer->id,'email'=>$data['email'],'created'=>date('Y-m-d h:is')]);
         StripeCustomerBilling::create(['stripeAccountID'=>$stripeAccount->stripeAccountID,
                                     'staffID'=>'',
                                     'status'=>'completed',
                                     'firstChargeDate'=>$data['firstPayment'],
                                     'totalCharges'=>0,
                                     'chargesLeft'=>0,
                                     'notes'=>''
                                     ]);*/
        }
    }
}