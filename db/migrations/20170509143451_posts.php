<?php

use Phinx\Migration\AbstractMigration;

class Posts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('Posts');
        $table->addColumn('title','string',['limit'=>100])
              ->addColumn('slug','string',['limit'=>10])
              ->addColumn('content','text')
              ->create();
        //insert data
        $data = [
          'title' => 'I love nutcrack php!',
          'slug' => 'nutcrack',
          'content' => 'lorem ipsum dolor'
        ];

        $table->insert($data);
        $table->saveData();
    }
}
